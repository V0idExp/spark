#pragma once

#include "script.h"

int
point_object_type_id();

Object*
point_object_new(int x, int y);

void
point_object_value_init(Value *v, int x, int y);

void
point_object_free(Object *o);

void
point_object_set(Object *o, int x, int y);

void
point_object_get(Object *o, int *x, int *y);

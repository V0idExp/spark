#include "spark.h"

#include "defines.h"
#include "internals.h"
#include "lua_engine.h"
#include "script.h"
#include <glib.h>

#define SYMBOL_PATTERN "[A-z][A-z0-9_]*"

extern struct Game game;

// Object types id counter
static int type_id_counter = SCRIPT_TYPE_OBJECT + 1;

// Mapping of object type ids to their TypeInfo
static GHashTable *types_table;

// GHashTable[type_id] -> GHashTable[name] -> MethodPrototype
static GHashTable *type_methods;

// Common script types registration routines
extern void
register_entity_type();

extern void
register_point_type();

extern void
register_array_type();

extern void
register_rect_type();

extern void
register_path_type();

extern void
register_tile_type();

/******************************************************************************
 * Hooks
******************************************************************************/
static void
new_object_hook(LuaEngine *le, Object *o)
{
	// attach methods
	GHashTable *methods = g_hash_table_lookup(
		type_methods, GINT_TO_POINTER(o->type)
	);
	if (methods) {
		GHashTableIter i;
		g_hash_table_iter_init(&i, methods);
		gpointer k, v;
		while (g_hash_table_iter_next(&i, &k, &v)) {
			MethodPrototype *proto = v;
			lua_engine_attach_method(
				game.lua_engine,
				o,
				(const char*)k,
				proto->method,
				proto->argc,
				proto->argtv
			);
		}
	}
}

static void
free_object_hook(LuaEngine *le, Object *o)
{
	// TODO
}

/******************************************************************************
 * Utility functions
******************************************************************************/
static void
free_hash_table(gpointer ptr)
{
	g_hash_table_destroy(ptr);
}

static void
free_method_prototype(gpointer ptr)
{
	g_free(((MethodPrototype*)ptr)->argtv);
	g_free(ptr);
}

/******************************************************************************
 * Script engine initialization, update and shutdown callbacks
******************************************************************************/
void
script_init()
{
	types_table = g_hash_table_new_full(NULL, NULL, NULL, g_free);
	type_methods = g_hash_table_new_full(
		NULL, NULL, NULL, free_hash_table
	);

	// initialize Lua engine
	game.lua_engine = lua_engine_new();

	// set hooks
	lua_engine_set_new_object_hook(game.lua_engine, new_object_hook);
	lua_engine_set_free_object_hook(game.lua_engine, free_object_hook);

	// register basic script types
	register_entity_type();
	register_array_type();
	register_point_type();
	register_rect_type();
	register_path_type();
	register_tile_type();
}

int
script_update()
{
	if (lua_engine_has_func(game.lua_engine, "update")) {
		return lua_engine_call(
			game.lua_engine, "update", 0, NULL
		).val.integer;
	}
	return 1;
}

void
script_exit()
{
	if (lua_engine_has_func(game.lua_engine, "exit"))
		lua_engine_call(game.lua_engine, "exit", 0, NULL);

	lua_engine_free(game.lua_engine);

	g_hash_table_destroy(type_methods);
	g_hash_table_destroy(types_table);
}

/******************************************************************************
 * API implementation
******************************************************************************/

EXPORT void
spark_script_run(const char *filename)
{
	// open the file as GIOChannel
	GError *err = NULL;
	GIOChannel *ch = g_io_channel_new_file(filename, "r", &err);
	if (!ch) {
		spark_log(
			LOG_FATAL,
			"(%s) unable to read file '%s': %s",
			g_quark_to_string(err->domain),
			filename,
			err->message
		);
	}

	// read the whole file at once
	gchar *data = NULL;
	gsize len;
	if (g_io_channel_read_to_end(ch, &data, &len, &err) != G_IO_STATUS_NORMAL) {
		g_io_channel_shutdown(ch, false, NULL);
		spark_log(LOG_FATAL, "failed to read file '%s'", filename);
	}

	// execute the script
	gchar *basename = g_path_get_basename(filename);
	if (lua_engine_run(game.lua_engine, basename, data) != 0) {
		char *err = lua_engine_get_error(game.lua_engine);
		spark_log(LOG_FATAL, "Lua error: %s", err);
	}
	g_free(basename);

	// if there's a init() function defined, call it
	if (lua_engine_has_func(game.lua_engine, "init")) {
		Value rval = lua_engine_call(game.lua_engine, "init", 0, NULL);
		if (rval.type == SCRIPT_TYPE_ERROR) {
			spark_log(LOG_FATAL, "Lua error: %s", rval.val.string);
		}
	}

	g_io_channel_shutdown(ch, false, NULL);
}

EXPORT int
spark_script_type_register(TypeInfo *type)
{
	int type_id = type_id_counter++;

	TypeInfo *i = g_memdup(type, sizeof(TypeInfo));
	g_hash_table_insert(types_table, GINT_TO_POINTER(type_id), i);

	return type_id;
}

EXPORT TypeInfo*
spark_script_type_info(int type_id)
{
	TypeInfo *i = g_hash_table_lookup(types_table, GINT_TO_POINTER(type_id));
	return i;
}

EXPORT const char*
spark_script_type_name(int type_id)
{
	TypeInfo *i = spark_script_type_info(type_id);
	if (i)
		return i->name;
	return "unknown";
}

EXPORT void
spark_script_func_register(
	const char *ns,
	const char *name,
	Func f,
	int argc,
	int *argtv
) {
	g_assert_nonnull(name);
	g_assert_nonnull(f);

	if (!g_regex_match_simple(SYMBOL_PATTERN, name, 0, 0)) {
		spark_log(LOG_FATAL, "invalid function name %s", name);
	}

	if (ns) {
		if (!g_regex_match_simple(SYMBOL_PATTERN, ns, 0, 0)) {
			spark_log(
				LOG_FATAL,
				"invalid namespace name %s",
				ns
			);
		}
		lua_engine_reg_lib_func(
			game.lua_engine, ns, name, f, argc, argtv
		);
	}
	else {
		lua_engine_reg_func(game.lua_engine, name, f, argc, argtv);
	}
}

EXPORT void
spark_script_type_method_register(
	int type_id,
	const char *name,
	Func f,
	int argc,
	int *argtv
) {
	g_assert_nonnull(name);
	g_assert_nonnull(f);
	g_assert_nonnull(argtv);

	if (!g_regex_match_simple(SYMBOL_PATTERN, name, 0, 0)) {
		spark_log(LOG_FATAL, "invalid method name %s", name);
	}

	if (argc < 1 || argtv[0] != SCRIPT_TYPE_OBJECT) {
		spark_log(
			LOG_FATAL,
			"the method must accept an Object as first argument"
		);
	}

	// lookup or create the methods table for given type
	GHashTable *methods = g_hash_table_lookup(
		type_methods, GUINT_TO_POINTER(type_id)
	);
	if (!methods) {
		methods = g_hash_table_new_full(
			g_str_hash,
			g_str_equal,
			g_free,
			free_method_prototype
		);
		g_hash_table_insert(
			type_methods, GUINT_TO_POINTER(type_id), methods
		);
	}

	// create a prototype descriptor and store it in methods table
	MethodPrototype *proto = g_new0(MethodPrototype, 1);
	proto->method = f;
	proto->argc = argc;
	proto->argtv = g_memdup(argtv, sizeof(int) * argc);

	g_hash_table_insert(methods, g_strdup(name), proto);
}

EXPORT Value
spark_script_func_call(const char *name, int argc, Value *argv)
{
	g_assert_nonnull(name);
	g_assert_true(argc >= 0);
	if (argc > 0)
		g_assert_nonnull(argv);

	return lua_engine_call(game.lua_engine, name, argc, argv);
}

EXPORT Value
spark_script_value_call(Value *v, int argc, Value *argv)
{
	g_assert_nonnull(v);
	g_assert_true(v->type == SCRIPT_TYPE_FUNCTION);
	g_assert_true(argc >= 0);
	if (argc > 0)
		g_assert_nonnull(argv);

	return lua_engine_call_by_ref(game.lua_engine, v->val.funcref, argc, argv);
}

EXPORT void
spark_script_object_attr_set(Object *o, const char *name, Value *attr)
{
	g_assert_nonnull(o);
	g_assert_nonnull(attr);
	lua_engine_set_attr(game.lua_engine, o->ref, name, attr);
}

EXPORT Value
spark_script_object_attr_get(Object *o, const char *name)
{
	g_assert_nonnull(o);
	g_assert_nonnull(name);
	return lua_engine_get_attr(game.lua_engine, o->ref, name);
}

EXPORT void
spark_script_object_prop_def(Object *o, const char *name, Property *prop)
{
	g_assert_nonnull(o);
	g_assert_nonnull(name);
	g_assert_nonnull(prop);
	g_assert_true(prop->type > SCRIPT_TYPE_NULL);

	lua_engine_define_property( game.lua_engine, o->ref, name, prop);
}

#include "script_types/entity.h"
#include "systems.h"
#include "systems/timer.h"
#include <glib.h>

static void
handle_timeout(Event *evt, void *data)
{
	Value f = {
		.type = SCRIPT_TYPE_FUNCTION,
		.val.funcref = GPOINTER_TO_INT(data)
	};

	spark_script_value_call(&f, 0, NULL);
}

static Value
start(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	int duration = argv[1].val.integer;
	timer_start(e, duration);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
on_timeout(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		TIMER,
		EVENT_TIMEOUT,
		e,
		handle_timeout,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

void
timer_system_register_wrappers()
{
	int argtv_duration[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_INT };
	entity_object_cmethod_register(
		TIMER,
		"start",
		start,
		2,
		argtv_duration
	);

	int argtv_callback[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_FUNCTION };
	entity_object_cmethod_register(
		TIMER,
		"on_timeout",
		on_timeout,
		2,
		argtv_callback
	);
}

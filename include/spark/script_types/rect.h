#pragma once

#include "common.h"
#include "script.h"

int
rect_object_type_id();

void
rect_object_value_init(Value *v, int x, int y, int w, int h);

Rect*
rect_object_get_rect(Object *o);

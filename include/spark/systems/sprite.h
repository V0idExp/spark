#pragma once

#include <spark.h>
#include <stdbool.h>

enum {
	EVENT_ANIMATION_FINISHED = 1
};

void
sprite_system_register();

void
sprite_init(Entity e, const char *sheet);

void
sprite_clone(Entity dst, Entity src);

void
sprite_position_set(Entity e, int x, int y);

void
sprite_position_get(Entity e, int *x, int *y);

void
sprite_frameset_set(Entity e, int *frameset, int count);

void
sprite_animation_add(
	Entity e,
	const char *name,
	int *frames,
	int count,
	int duration
);

void
sprite_animation_play(Entity e, const char *name);

void
sprite_animation_pause(Entity e);

bool
sprite_animation_is_paused(Entity e);

void
sprite_animation_resume(Entity e);

void
sprite_visible_set(Entity e, bool visible);

bool
sprite_visible_get(Entity e);

void
sprite_zorder_set(Entity e, int z);

int
sprite_zorder_get(Entity e);

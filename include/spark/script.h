/**
 * @file
 * @defgroup script script
 * Scripting API
 * @{
 */

#pragma once

enum {
	SCRIPT_TYPE_ERROR = -2,
	SCRIPT_TYPE_UNKNOWN,
	SCRIPT_TYPE_NULL,
	SCRIPT_TYPE_BOOL,
	SCRIPT_TYPE_FLOAT,
	SCRIPT_TYPE_INT,
	SCRIPT_TYPE_STRING,
	SCRIPT_TYPE_VARARG,
	SCRIPT_TYPE_OBJECT,
	SCRIPT_TYPE_FUNCTION
};

/**
 * Instance of an object of a particular type.
 */
typedef struct Object {
	// identifier associated with object's TypeInfo
	int type;
	// object's data
	void *data;
	// script engine reference, which identifies the object uniquely
	int ref;
} Object;

/**
 * Generic script value container.
 */
typedef struct Value {
	int type;

	union {
		int integer;
		float real;
		char *string;
		Object object;
		int funcref;
	} val;
} Value;

/**
 * Object type info.
 */
typedef struct TypeInfo {
	/**
	 * Name with which the type will be visible to scripts.
	 * MUST BE A STATIC STRING.
	 */
	char *name;

	/**
	 * Constructor.
	 *
	 * Called by script engine when a value of this type is created.
	 */
	void (*ctor)(Object *self);

	/**
	 * Destructor.
	 *
	 * Called by script engine before releasing the value.
	 */
	void (*dtor)(Object *self);

	/**
	 * Stringifier.
	 *
	 * If present, is expected to return a newly-allocated user-friendly
	 * string representation of the object instance.
	 */
	char* (*str)(Object *self);
} TypeInfo;

/**
 * Prototype of a function or method callable by the script engine.
 */
typedef Value (*Func)(int argc, Value *argv);

/**
 * Property setter function.
 */
typedef Value (*PropSetterFunc)(Object *o, Value *v);

/**
 * Property getter function.
 */
typedef Value (*PropGetterFunc)(Object *o);

/**
 * Property definition
 */
typedef struct Property {
	int type;
	PropSetterFunc setter;
	PropGetterFunc getter;
} Property;

/**
 * Register a function in the given namespace.
 */
void
spark_script_func_register(
	const char *ns,
	const char *name,
	Func f,
	int argc,
	int *argtv
);

/**
 * Register a new type.
 */
int
spark_script_type_register(TypeInfo *type);

/**
 * Register a type method.
 */
void
spark_script_type_method_register(
	int type_id,
	const char *name,
	Func f,
	int argc,
	int *argtv
);

/**
 * Get the type information.
 */
TypeInfo*
spark_script_type_info(int type_id);

/**
 * Get the type name.
 */
const char*
spark_script_type_name(int type_id);

/**
 * Execute a script.
 *
 * Loads, compiles and executes the given script file.
 * Following callbacks are looked up and called (if present):
 *
 *   init()    called just after a successfull compilation
 *   update()  called at the beginning of each game loop iteration
 *   exit()    called at exit
 */
void
spark_script_run(const char *filename);

/**
 * Call a script function by name.
 */
Value
spark_script_func_call(const char *name, int argc, Value *argv);

/**
 * Call a function stored in a value.
 */
Value
spark_script_value_call(Value *v, int argc, Value *argv);

/**
 * Set an attribute to the object.
 */
void
spark_script_object_attr_set(Object *o, const char *name, Value *attr);

/**
 * Get an object attribute.
 *
 * If there's no attribute with given name, will return NULL.
 */
Value
spark_script_object_attr_get(Object *o, const char *name);

/**
 * Define a property as object attribute.
 */
void
spark_script_object_prop_def(Object *o, const char *name, Property *prop_def);

/** @} */

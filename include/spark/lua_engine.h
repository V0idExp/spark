#pragma once

#include "script.h"
#include <stdbool.h>

typedef struct LuaEngine LuaEngine;

typedef void (*LuaEngineHook)(LuaEngine *le, Object *o);

LuaEngine*
lua_engine_new();

void
lua_engine_free(LuaEngine *le);

int
lua_engine_run(LuaEngine *le, const char *name, const char *code);

char*
lua_engine_get_error(LuaEngine *le);

void
lua_engine_reg_func(
	LuaEngine *le,
	const char *name,
	Func f,
	int argc,
	int *argtv
);

int
lua_engine_reg_lib_func(
	LuaEngine *le,
	const char *libname,
	const char *name,
	Func f,
	int argc,
	int *argtv
);

void
lua_engine_set_attr(LuaEngine *le, int ref, const char *name, Value *attr);

Value
lua_engine_get_attr(LuaEngine *le, int ref, const char *name);

void
lua_engine_define_property(
	LuaEngine *le,
	int ref,
	const char *name,
	Property *prop
);

void
lua_engine_attach_method(
	LuaEngine *le,
	Object *o,
	const char *name,
	Func f,
	int argc,
	int *argtv
);

void
lua_engine_detach_method(LuaEngine *le, Object *o, const char *name);

Value
lua_engine_call(LuaEngine *le, const char *func, int argc, Value *argv);

Value
lua_engine_call_by_ref(LuaEngine *le, int funcref, int argc, Value *argv);

bool
lua_engine_has_func(LuaEngine *le, const char *func);

void
lua_engine_set_new_object_hook(LuaEngine *le, LuaEngineHook f);

void
lua_engine_set_free_object_hook(LuaEngine *le, LuaEngineHook f);

#pragma once

#include "common.h"
#include "refcounter.h"

typedef struct Tile {
	Entity tilemap;
	int row;
	int col;
	Rect rect;
	void *data;
} Tile;

enum {
	TILE_CREATE,
	TILE_UPDATE,
	TILE_DESTROY
};

typedef void (TileUpdateFunc)(int update_type, struct Tile *t, void *user_data);

void
tilemap_system_register();

void
tilemap_init(
	Entity entity,
	unsigned short tile_width,
	unsigned short tile_height,
	unsigned int cols,
	unsigned int rows,
	TileUpdateFunc *update,
	RefCounter *user_data
);

void
tilemap_position_set(Entity entity, int left, int top);

struct Tile*
tilemap_tile_at_point(Entity entity, int x, int y);

unsigned int
tilemap_tile_width_get(Entity entity);

unsigned int
tilemap_tile_height_get(Entity entity);

unsigned int
tilemap_n_rows_get(Entity entity);

unsigned int
tilemap_n_cols_get(Entity entity);

void
tilemap_tile_update(Entity entity, int col, int row);

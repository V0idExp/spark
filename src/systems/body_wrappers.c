#include "script_types/entity.h"
#include "script_types/rect.h"
#include "systems/body.h"
#include <glib.h>
#include <string.h>

#define reg_wrapper(f, ...) {\
	char *name = #f;\
	int argtv[] = { SCRIPT_TYPE_OBJECT, __VA_ARGS__ };\
	int argc = sizeof(argtv) / sizeof(int);\
	entity_object_cmethod_register(BODY, name + 5, f, argc, argtv);\
}

#define reg_property(name, t) {\
	Property def = {\
		.type = t,\
		.setter = name ## _setter,\
		.getter = name ## _getter\
	};\
	entity_object_cproperty_register(BODY, #name, &def);\
}

#define reg_readonly_property(name, t) {\
	Property def = {\
		.type = t,\
		.getter = name ## _getter\
	};\
	entity_object_cproperty_register(BODY, #name, &def);\
}

static Value
wrap_init(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};

	// get body type arg
	int body_type;
	if (strncmp(argv[1].val.string, "static", strlen("static")) == 0) {
		body_type = BODY_TYPE_STATIC;
	}
	else if (strncmp(argv[1].val.string, "movable", strlen("movable")) == 0) {
		body_type = BODY_TYPE_MOVABLE;
	}
	else {
		retval.val.string = g_strdup(
			"expected body type as first argument ('static' or 'movable')"
		);
		goto bad_args;
	}

	// get bounding rect arg
	if (argv[2].val.object.type != rect_object_type_id()) {
		retval.val.string = g_strdup(
			"expected a Rect object as second argument"
		);
		goto bad_args;
	}
	Rect rect = *rect_object_get_rect(&argv[2].val.object);

	// get the body group
	int group = argv[3].val.integer;
	if (group < 0) {
		retval.val.string = g_strdup(
			"expected a positive group number as third argument"
		);
		goto bad_args;
	}

	body_init(e, body_type, &rect, group);

	return retval;

bad_args:
	retval.type = SCRIPT_TYPE_ERROR;
	return retval;
}

static Value
x_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);

	int x, y;
	body_position_get(e, &x, &y);
	body_position_set(e, v->val.integer, y);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
x_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);

	int x, y;
	body_position_get(e, &x, &y);

	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = x
	};
	return retval;
}

static Value
y_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);

	int x, y;
	body_position_get(e, &x, &y);
	body_position_set(e, x, v->val.integer);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
y_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);

	int x, y;
	body_position_get(e, &x, &y);

	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = y
	};
	return retval;
}

static Value
rect_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	Rect *r = body_rect(e);
	Value retval;
	rect_object_value_init(
		&retval, r->left, r->top, rect_ptr_width(r), rect_ptr_height(r)
	);

	return retval;
}

static Value
type_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = body_type(e)
	};

	return retval;
}

static Value
group_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = body_group(e)
	};

	return retval;
}

static void
handle_collision(Event *evt, void *data)
{
	Value f = {
		.type = SCRIPT_TYPE_FUNCTION,
		.val.funcref = GPOINTER_TO_INT(data)
	};
	Value arg;
	entity_object_value_init(&arg, evt->entity);

	spark_script_value_call(&f, 1, &arg);
}

static Value
wrap_on_collision(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		BODY,
		EVENT_COLLISION,
		e,
		handle_collision,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

void
body_system_wrappers_register()
{
	reg_wrapper(wrap_init, SCRIPT_TYPE_STRING, SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_INT);
	reg_wrapper(wrap_on_collision, SCRIPT_TYPE_FUNCTION);
	reg_property(x, SCRIPT_TYPE_INT);
	reg_property(y, SCRIPT_TYPE_INT);
	reg_readonly_property(rect, SCRIPT_TYPE_OBJECT);
	reg_readonly_property(type, SCRIPT_TYPE_INT);
	reg_readonly_property(group, SCRIPT_TYPE_INT);
}


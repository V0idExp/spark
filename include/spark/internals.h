#pragma once

#include "containers/pool.h"
#include "defines.h"
#include <glib.h>

#if defined(HAS_LUA)
#include "lua_engine.h"
#endif

struct HookInfo {
	Hook hook;
	void *userdata;
};

struct Hooks {
	GArray *entity_created;
	GArray *entity_destroyed;
};

struct Game {
	GHashTable *systems;
	GHashTable *sys_names_map;
	GArray *reg_order;
	Pool *event_queue;
	Pool *entities;
	int log_level;
	GHashTable *subscriptions;
	GKeyFile *cfg;
	struct Hooks hooks;

#if defined(HAS_LUA)
	LuaEngine *lua_engine;
#endif
} game;

struct EventSubscription {
	EventHandler handler;
	void *data;
};

typedef struct EntityInfo {
	Entity entity;
	GHashTable *components;
} EntityInfo;

typedef struct MethodPrototype {
	int argc;
	int *argtv;
	Func method;
} MethodPrototype;

void
dispatch_events();

void
unsubscribe_entity(Entity e);

void
unsubscribe_entity_component(Entity e, ComponentType t);

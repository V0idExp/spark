#pragma once

#include "systems.h"
#include "util.h"
#include <SDL_platform.h>

// Rect type
#ifdef __MACOSX__
#include <MacTypes.h>
#else
typedef struct {
	short top, left, bottom, right;
} Rect;
#endif

#define rect_width(__rect) (__rect.right - __rect.left)
#define rect_height(__rect) (__rect.bottom - __rect.top)
#define rect_ptr_width(__rect) (__rect->right - __rect->left)
#define rect_ptr_height(__rect) (__rect->bottom - __rect->top)

#include "spark.h"
#include "defines.h"
#include "script_types/array.h"
#include <glib.h>
#include <stdbool.h>

static int array_type_id = 0;

static void
array_ctor(Object *self)
{
	self->type = array_type_id;
	if (!self->data)
		self->data = g_array_new(false, false, sizeof(Value));
}

static void
array_dtor(Object *self)
{
	g_array_free(self->data, true);
}

static char*
array_str(Object *self)
{
	return g_strdup_printf("Array");
}

static Value
array_new(int argc, Value *argv)
{
	Value retval;
	array_object_value_init(&retval, argc, argv);

	return retval;
}

EXPORT int
array_object_type_id()
{
	return array_type_id;
}

EXPORT void
array_object_value_init(Value *v, int len, Value *elements)
{
	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.type = array_type_id;
	v->val.object.data = NULL;

	array_ctor(&v->val.object);

	for (int i = 0; i < len; i++) {
		g_array_append_val(v->val.object.data, elements[i]);
	}
}

EXPORT Value*
array_object_index(Object *o, int index)
{
	GArray *array = o->data;
	if (index >= array->len) {
		spark_log(LOG_FATAL, "array index out of range");
	}

	return &g_array_index(array, Value, index);
}

EXPORT size_t
array_object_len(Object *o)
{
	GArray *array = o->data;
	return array->len;
}

EXPORT int
array_object_has_type(Object *o, int type, int obj_type)
{
	GArray *array = o->data;
	for (int i = 0; i < array->len; i++) {
		Value *v = &g_array_index(array, Value, i);
		if (v->type != type || (obj_type != 0 && v->val.object.type != obj_type))
			return 0;
	}
	return 1;
}

void
register_array_type()
{
	TypeInfo ti = {
		.name = "Array",
		.ctor = array_ctor,
		.dtor = array_dtor,
		.str = array_str
	};

	array_type_id = spark_script_type_register(&ti);

	int argtv[] = { SCRIPT_TYPE_VARARG };
	spark_script_func_register("Array", "new", array_new, 1, argtv);
}

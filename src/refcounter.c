#include "refcounter.h"
#include <glib.h>
#include <stdlib.h>

struct RefCounter{
	void *data;
	unsigned int *count;
	void (*destructor)(void*);
};


RefCounter*
ref_counter_new(void *data, void (*destructor)(void*))
{
	RefCounter *rc = g_new(RefCounter, 1);
	rc->count = g_new(unsigned int, 1);
	rc->data = data;
	rc->destructor = destructor != NULL ? destructor : free;

	++(*rc->count);

	return rc;
}

RefCounter*
ref_counter_inc(RefCounter *rc)
{
	RefCounter *copy = g_memdup(rc, sizeof(RefCounter));
	++(*rc->count);

	return copy;
}

void
ref_counter_dec(RefCounter *rc)
{
	if (--(*rc->count) == 0) {
		g_free(rc->count);
		rc->destructor(rc->data);
	}

	g_free(rc);
}

void*
ref_counter_get(RefCounter *rc)
{
	return rc->data;
}


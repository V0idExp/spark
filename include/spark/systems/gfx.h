#pragma once

#include <SDL_render.h>
#include <SDL_video.h>

void
gfx_system_register();

SDL_Window*
gfx_system_get_window(struct System *gfx_system);

SDL_Renderer*
gfx_system_get_renderer(struct System *gfx_system);

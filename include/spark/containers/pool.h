/**
 * @file
 * @defgroup containers containers
 * Useful data structures
 * @{
 * @defgroup Pool
 * @{
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

/**
 * Memory pool of objects of same size.
 */
typedef struct Pool Pool;

/**
 * Pool iterator.
 */
typedef struct {
	Pool *p;
	size_t i;
} PoolIter;

/**
 * Create a pool.
 */
Pool*
pool_new(size_t element_size, int count, int grow_by);

/**
 * Destroy a pool.
 */
void
pool_free(Pool *p);

/**
 * Get the number of elements in pool.
 */
size_t
pool_len(Pool *p);

/**
 * Get the actual pool size.
 */
size_t
pool_size(Pool *p);

/**
 * Get the size by which pool grows.
 */
size_t
pool_grow_size(Pool *p);

/**
 * Reserve space for one more element.
 */
int
pool_reserve(Pool *p);

/**
 * Set the element at given index.
 */
void
pool_set(Pool *p, int index, const void *data);

/**
 * Add an element to pool.
 */
int
pool_add(Pool *p, const void *data);

/**
 * Get the element at given index.
 */
void*
pool_get(Pool *p, int index);

/**
 * Remove the element at given index.
 */
void
pool_remove(Pool *p, int index);

/**
 * Initialize pool iterator.
 */
void
pool_iter_init(Pool *p, PoolIter *i);

/**
 * Iterate next.
 */
bool
pool_iter_next(PoolIter *iter);

/**
 * Get the element from iterator.
 */
void*
pool_iter_get(PoolIter *iter);

/**
 * Get the index of iterator.
 */
int
pool_iter_index(PoolIter *iter);

/**
 * @}
 * @}
 */

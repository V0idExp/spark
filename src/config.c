#include "spark.h"
#include "config.h"
#include "internals.h"

#define handle_err(error)\
	if (error && error->code == G_KEY_FILE_ERROR_INVALID_VALUE)\
		spark_log(LOG_FATAL, error->message);

char*
spark_config_get_string(const char *section, const char *key)
{
	GError *err = NULL;
	char *s = g_key_file_get_string(game.cfg, section, key, &err);
	handle_err(err);
	return s;
}

int
spark_config_get_int(const char *section, const char *key)
{
	GError *err = NULL;
	int i = g_key_file_get_integer(game.cfg, section, key, &err);
	handle_err(err);
	return i;
}

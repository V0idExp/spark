#pragma once

/**
 * Get a string value from config.
 */
char*
spark_config_get_string(const char *section, const char *key);

/**
 * Get an integer from config.
 */
int
spark_config_get_int(const char *section, const char *key);

#include "containers/mapped_pool.h"
#include <glib.h>
#include <string.h>

static void
setup(MappedPool **mp, gconstpointer user_data)
{
	*mp = mapped_pool_new(1, 10, 5);
}

static void
teardown(MappedPool **mp, gconstpointer user_data)
{
	mapped_pool_free(*mp);
}

static void
test_create(MappedPool **mp, gconstpointer user_data)
{
	Pool *p = mapped_pool_storage_get(*mp);
	g_assert_cmpuint(pool_len(p), ==, 0);
	g_assert_cmpuint(pool_size(p), ==, 10);
}

static void
test_set(MappedPool **mp, gconstpointer user_data)
{
	char a = 'a', b = 'b';
	Pool *p = mapped_pool_storage_get(*mp);

	mapped_pool_set(*mp, 123, &a);
	g_assert_cmpuint(pool_len(p), ==, 1);

	mapped_pool_set(*mp, 321, &b);
	g_assert_cmpuint(pool_len(p), ==, 2);
}

static void
test_pop(MappedPool **mp, gconstpointer user_data)
{
	Pool *p = mapped_pool_storage_get(*mp);

	mapped_pool_set(*mp, 123, "a");
	g_assert_cmpuint(pool_len(p), ==, 1);

	g_assert_true(mapped_pool_pop(*mp, 123));
	g_assert_cmpuint(pool_len(p), ==, 0);
}

static void
test_get(MappedPool **mp, gconstpointer user_data)
{
	g_assert_null(mapped_pool_get(*mp, 111));
	g_assert_true(mapped_pool_set(*mp, 111, "a"));
	g_assert_cmpuint((*(char*)mapped_pool_get(*mp, 111)), ==, 'a');
}

static void
test_grow(MappedPool **mp, gconstpointer user_data)
{
	for (int i = 0, a = 'a'; i < 10; i++, a++) {
		mapped_pool_set(*mp, i, &a);
	}

	Pool *p = mapped_pool_storage_get(*mp);
	g_assert_cmpuint(pool_len(p), ==, 10);
	g_assert_cmpuint(pool_size(p), ==, 10);

	mapped_pool_set(*mp, 11, "l");
	g_assert_cmpuint(pool_len(p), ==, 11);
	g_assert_cmpuint(pool_size(p), ==, 15);
}

void
mapped_pool_suite_setup()
{
	g_test_add(
		"/mapped_pool/create",
		MappedPool*,
		NULL,
		setup,
		test_create,
		teardown
	);

	g_test_add(
		"/mapped_pool/set",
		MappedPool*,
		NULL,
		setup,
		test_set,
		teardown
	);

	g_test_add(
		"/mapped_pool/pop",
		MappedPool*,
		NULL,
		setup,
		test_pop,
		teardown
	);

	g_test_add(
		"/mapped_pool/get",
		MappedPool*,
		NULL,
		setup,
		test_get,
		teardown
	);

	g_test_add(
		"/mapped_pool/grow",
		MappedPool*,
		NULL,
		setup,
		test_grow,
		teardown
	);
}

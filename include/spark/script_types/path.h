#pragma once

#include "script.h"
#include "spark/path.h"

int
path_object_type_id();

void
path_object_value_init(Value *v, Path *path);

Path*
path_object_data(Object *o);

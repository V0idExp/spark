#include "common.h"
#include "systems/image.h"
#include <SDL_image.h>
#include <SDL_video.h>
#include <glib.h>

static void
image_system_init(struct System *self)
{
	int init_formats = IMG_INIT_PNG;
	if (IMG_Init(init_formats) != init_formats)
		spark_log(LOG_FATAL, "failed to initialize SDL_image");
}

static void
image_system_exit(struct System *self)
{
	IMG_Quit();
}

struct SDL_Surface*
image_load(struct System *sys, const char *filename)
{
	SDL_Surface *img = IMG_Load(filename);
	if (!img) {
		spark_log(
			LOG_FATAL,
			"failed to load image '%s': %s",
			filename,
			IMG_GetError()
		);
	}

	spark_log(LOG_INFO, "loaded image '%s'", filename);
	return img;
}

void
image_system_register()
{
	spark_assert_has_systems(CORE);

	struct System *sys = g_new0(struct System, 1);
	sys->name = "image";
	sys->init = image_system_init;
	sys->exit = image_system_exit;
	spark_system_register(IMAGE, sys);
}

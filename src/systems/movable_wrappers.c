#include "script.h"
#include "script_types/entity.h"
#include "script_types/path.h"
#include "script_types/point.h"
#include "spark/systems/movable.h"
#include <glib.h>

#define reg_property(name, t) {\
	Property def = {\
		.type = t,\
		.setter = name ## _setter,\
		.getter = name ## _getter\
	};\
	entity_object_cproperty_register(MOVABLE, #name, &def);\
}

#define reg_ro_property(name, t) {\
	Property def = {\
		.type = t,\
		.getter = name ## _getter\
	};\
	entity_object_cproperty_register(MOVABLE, #name, &def);\
}

static Value
path_set(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};

	if (argv[1].val.object.type != path_object_type_id()) {
		retval.type = SCRIPT_TYPE_ERROR;
		retval.val.string = g_strdup("path_set() requires a Path object");
		return retval;
	}

	movable_path_set(e, path_object_data(&argv[1].val.object));
	return retval;
}

static Value
path_update(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};

	if (argv[1].val.object.type != path_object_type_id()) {
		retval.type = SCRIPT_TYPE_ERROR;
		retval.val.string = g_strdup("path_set() requires a Path object");
		return retval;
	}

	movable_path_update(e, path_object_data(&argv[1].val.object));
	return retval;
}

static Value
speed_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};

	if (v->val.integer < 0) {
		retval.type = SCRIPT_TYPE_ERROR;
		retval.val.string = g_strdup("property requires a positive integer");
		return retval;
	}

	movable_speed_set(e, v->val.integer);
	return retval;
}

static Value
speed_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	unsigned short speed = movable_speed_get(e);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = speed
	};
	return retval;
}

static void
notify(Event *evt, void *data)
{
	MovableUpdateInfo *mui = evt->data;
	Value pos;
	point_object_value_init(&pos, mui->x, mui->y);

	Value f = {
		.type = SCRIPT_TYPE_FUNCTION,
		.val.funcref = GPOINTER_TO_INT(data)
	};
	spark_script_value_call(&f, 1, &pos);
}

static Value
on_position_change(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		MOVABLE,
		EVENT_MOVABLE_POSITION_CHANGE,
		e,
		notify,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
on_node_reach(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		MOVABLE,
		EVENT_MOVABLE_NODE_REACH,
		e,
		notify,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
x_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	movable_x_set(e, v->val.integer);
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
x_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	int x = movable_x_get(e);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = x
	};
	return retval;
}

static Value
y_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	movable_y_set(e, v->val.integer);
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
y_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	int y = movable_y_get(e);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = y
	};
	return retval;
}

static Value
direction_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	double direction = movable_direction_get(e);
	Value retval = {
		.type = SCRIPT_TYPE_FLOAT,
		.val.real = direction
	};
	return retval;
}

void
movable_system_register_wrappers()
{
	reg_property(speed, SCRIPT_TYPE_INT);
	reg_property(x, SCRIPT_TYPE_INT);
	reg_property(y, SCRIPT_TYPE_INT);
	reg_ro_property(direction, SCRIPT_TYPE_FLOAT);

	int argtv_path[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_OBJECT };
	entity_object_cmethod_register(
		MOVABLE,
		"set_path",
		path_set,
		2,
		argtv_path
	);
	entity_object_cmethod_register(
		MOVABLE,
		"update_path",
		path_update,
		2,
		argtv_path
	);

	int argtv[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_FUNCTION };
	entity_object_cmethod_register(
		MOVABLE,
		"on_position_change",
		on_position_change,
		2,
		argtv
	);
	entity_object_cmethod_register(
		MOVABLE,
		"on_node_reach",
		on_node_reach,
		2,
		argtv
	);
}

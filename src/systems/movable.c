#include "common.h"
#include "containers/mapped_pool.h"
#include "systems/movable.h"
#include <SDL_timer.h>
#include <glib.h>
#include <math.h>

typedef struct Movable {
	Entity entity;
	Path *path;
	Path *next_path;
	unsigned short speed;
	int x;
	int y;
	int dst_x;
	int dst_y;
	int current_node;

	Uint32 last_update;
	double accumulator;
	double acc_x;
	double acc_y;
	double direction;
} Movable;


static void
movable_system_init(System *self)
{
	self->data = mapped_pool_new(sizeof(Movable), 100, 50);
}

static void
movable_system_exit(System *self)
{
	mapped_pool_free(self->data);
}

static void
movable_system_create_component(System *self, Entity entity)
{
	Movable m = { 0 };
	m.entity = entity;
	mapped_pool_set(self->data, entity, &m);
}

static void*
movable_system_get_component(System *self, Entity entity)
{
	return mapped_pool_get(self->data, entity);
}

static void
movable_system_destroy_component(System *self, Entity entity)
{
	mapped_pool_pop(self->data, entity);
}

static void
movable_system_update(System *self)
{
	Pool *p = mapped_pool_storage_get(self->data);
	PoolIter i;
	pool_iter_init(p, &i);

	while (pool_iter_next(&i)) {
		Movable *m = pool_iter_get(&i);

		if (m->next_path) {
			m->path = m->next_path;
			m->next_path = NULL;
			m->current_node = 0;
		}

		// skip movables which have no path to follow
		if (!m->path)
			continue;

		// compute the unit time
		double dt = 1000.0 / m->speed;

		// compute the frame time and update the accumulator
		Uint32 now = SDL_GetTicks();
		if (!m->last_update) {
			m->last_update = now;
			m->accumulator = 0.0;
		}
		m->accumulator += now - m->last_update;
		m->last_update = now;

		PathNode *node = NULL;

		// consume accumulator in dt chunks and update the position one
		// unit at time
		while (m->accumulator >= dt) {

			m->accumulator -= dt;

			node = path_node_get(
				m->path, m->current_node
			);

			// switch to next node or stop movement
			if (m->x == node->x && m->y == node->y) {
				// notify about node reach
				MovableUpdateInfo upd = {
					.x = m->x,
					.y = m->y,
					.node_index = m->current_node
				};
				spark_event_push(
					MOVABLE,
					EVENT_MOVABLE_NODE_REACH,
					m->entity,
					&upd,
					sizeof(MovableUpdateInfo)
				);

				if (++(m->current_node) < path_len(m->path)) {
					node = path_node_get(
						m->path, m->current_node
					);
				}
				else {
					m->path = NULL;
					m->direction = 0;
					break;
				}
			}

			int dx = node->x - m->x;
			int dy = node->y - m->y;

			// retrieve the angle
			m->direction = atan2((float)dy, (float)dx);

			// update accumulators
			m->acc_x += cos(m->direction);
			m->acc_y += sin(m->direction);

			// update the position
			if (m->acc_x && fabs(m->acc_x) >= 1) {
				m->x += m->acc_x > 0 ? 1 : -1;
				m->acc_x -= m->acc_x > 0 ? 1 : -1;
			}
			if (m->acc_y && fabs(m->acc_y) >= 1) {
				m->y += m->acc_y > 0 ? 1 : -1;
				m->acc_y -= m->acc_y > 0 ? 1 : -1;
			}
		}

		// notify about position update, if there was any
		if (node) {
			MovableUpdateInfo upd = {
				.x = m->x,
				.y = m->y,
				.node_index = m->current_node,
			};
			spark_event_push(
				MOVABLE,
				EVENT_MOVABLE_POSITION_CHANGE,
				m->entity,
				&upd,
				sizeof(MovableUpdateInfo)
			);
		}
	}
}

extern void
movable_system_register_wrappers();

void
movable_system_register()
{
	spark_assert_has_systems(CORE);

	System *sys = g_new0(System, 1);
	sys->name = "movable";
	sys->init = movable_system_init;
	sys->exit = movable_system_exit;
	sys->update = movable_system_update;
	sys->create_component = movable_system_create_component;
	sys->get_component = movable_system_get_component;
	sys->destroy_component = movable_system_destroy_component;

	spark_system_register(MOVABLE, sys);

	movable_system_register_wrappers();
}

void
movable_path_set(Entity entity, Path *path)
{
	g_assert_nonnull(path);
	g_assert_true(path_len(path) > 0);

	Movable *m = spark_entity_component_get(entity, MOVABLE);
	// TODO: free the path?
	m->path = path;

	PathNode *start = path_node_get(path, 0);
	m->x = start->x;
	m->y = start->y;
	m->current_node = m->last_update = 0;
	m->accumulator = 0.0;
}

void
movable_path_update(Entity entity, Path *path)
{
	g_assert_nonnull(path);
	if (path_len(path) == 0)
		return;

	Movable *m = spark_entity_component_get(entity, MOVABLE);

	Path *new_path = path_new();
	PathNode start = {
		.x = m->x,
		.y = m->y
	};
	path_node_add(new_path, start);
	for (int i = 0; i < path_len(path); ++i)
		path_node_add(new_path, *path_node_get(path, i));

	if (!m->path)
		movable_path_set(entity, new_path);
	else
		m->next_path = new_path;
}

Path*
movable_path_get(Entity e)
{
	return ((Movable*)spark_entity_component_get(e, MOVABLE))->path;
}

void
movable_path_clear(Entity entity)
{
	Movable *m = spark_entity_component_get(entity, MOVABLE);
	// TODO: free the path?
	m->path = NULL;
}

void
movable_speed_set(Entity entity, unsigned short speed)
{
	Movable *m = spark_entity_component_get(entity, MOVABLE);
	m->speed = speed;
}

unsigned short
movable_speed_get(Entity e)
{
	return ((Movable*)spark_entity_component_get(e, MOVABLE))->speed;
}

void
movable_x_set(Entity entity, int x)
{
	Movable *m = spark_entity_component_get(entity, MOVABLE);
	m->x = x;
}

int
movable_x_get(Entity e)
{
	return ((Movable*)spark_entity_component_get(e, MOVABLE))->x;
}

void
movable_y_set(Entity entity, int y)
{
	Movable *m = spark_entity_component_get(entity, MOVABLE);
	m->y = y;
}

int
movable_y_get(Entity e)
{
	return ((Movable*)spark_entity_component_get(e, MOVABLE))->y;
}

double
movable_direction_get(Entity e)
{
	return ((Movable*)spark_entity_component_get(e, MOVABLE))->direction;
}

#include "loaders/tmx_map_loader.h"
#include "script.h"
#include "spark/script_types/entity.h"
#include "spark/script_types/tile.h"
#include "spark/systems.h"
#include "spark/systems/sprite.h"
#include <glib.h>
#include <tmx.h>

static bool registered = false;

typedef struct TmxMapInfo {
	tmx_map *map;
	tmx_tileset *ts;
	tmx_layer *ly;
	TileUpdateFunc *f;
	RefCounter *userdata_rc;
	char **anim_names;
} TmxMapInfo;

static void
destroy_tmx_map_info(void *data)
{
	TmxMapInfo *map_info = data;
	tmx_map_free(map_info->map);
	if (map_info->userdata_rc)
		ref_counter_dec(map_info->userdata_rc);
	g_strfreev(map_info->anim_names);
}

static void
update_func(int update_type, struct Tile *t, void *data)
{
	TmxMapInfo *info = data;
	TMXTile *tile_data = t->data;

	if (update_type == TILE_CREATE) {
		// find the id of the tile in the tilesheet
		int index = t->row * info->map->width + t->col;
		unsigned int gid = info->ly->content.gids[index] & TMX_FLIP_BITS_REMOVAL;
		int id = gid - info->ts->firstgid;

		// create a new entity with a sprite component
		ComponentType components[] = { SPRITE, 0 };
		Entity tile_entity = spark_entity_create(components);
		sprite_clone(tile_entity, t->tilemap);

		// play the related animation
		sprite_animation_play(tile_entity, info->anim_names[id]);

		// make it visible
		sprite_visible_set(tile_entity, true);

		// initialize additional data
		tile_data = g_new0(TMXTile, 1);
		tile_data->entity = tile_entity;

		// store properties, if any
		tile_data->props = g_hash_table_new(g_str_hash, g_str_equal);
		tmx_tile *raw_tile = tmx_get_tile(info->map, gid);
		if (raw_tile) {
			for (tmx_property *p = raw_tile->properties; p; p = p->next) {
				g_hash_table_insert(tile_data->props, p->name, p->value);
			}
		}

		t->data = tile_data;
	}
	else if (update_type == TILE_DESTROY) {
		if (info->f)
			info->f(update_type, t, ref_counter_get(info->userdata_rc));

		spark_entity_destroy(tile_data->entity);

		g_hash_table_destroy(tile_data->props);
		g_free(tile_data);
		t->data = NULL;
		return;
	}

	// position the sprite to the center of tile rect
	int x = t->rect.left + info->ts->tile_width / 2;
	int y = t->rect.top + info->ts->tile_height / 2;
	sprite_position_set(tile_data->entity, x, y);

	if (info->f)
		info->f(update_type, t, ref_counter_get(info->userdata_rc));
}

Entity
tmx_map_load(
	const char *filename,
	TileUpdateFunc f,
	RefCounter *userdata_rc
) {
	g_assert_nonnull(filename);

	if (!registered) {
		spark_log(LOG_FATAL, "TMX map loader must be registered first");
	}

	// create the tilemap entity
	spark_assert_has_systems(TILEMAP);
	ComponentType components[] = { TILEMAP, 0};
	Entity e = spark_entity_create(components);

	// load the TMX map
	tmx_map *map = tmx_load(filename);
	if (!map)
		spark_log(LOG_FATAL, "failed to load TMX map '%s'", filename);

	tmx_tileset *ts = map->ts_head;
	tmx_layer *ly = map->ly_head;

	// assert we have ony one tileset
	if (!ts || ts->next)
		spark_log(LOG_FATAL, "TMX map '%s' must have exactly one tileset", filename);

	// assert we have only one layer
	if (!ly || ly->next || ly->type != L_LAYER)
		spark_log(LOG_FATAL, "TMX map '%s' must have exactly one layer", filename);

	// initialize a TmxMapInfo struct
	TmxMapInfo *info = g_new0(TmxMapInfo, 1);
	info->map = map;
	info->ts = ts;
	info->ly = ly;
	info->f = f;
	info->userdata_rc = userdata_rc;
	info->anim_names = NULL;

	if (userdata_rc)
		ref_counter_inc(userdata_rc);

	// attach the sprite component if we have an image associated with the
	// tileset
	if (ts->image) {
		tmx_image *img = ts->image;
		spark_assert_has_systems(SPRITE);
		spark_entity_component_add(e, SPRITE);

		// initialize sprite
		char *filename = g_build_filename("data", img->source, NULL);
		sprite_init(e, filename);
		g_free(filename);

		// the master sprite is hidden; tiles will set their visibility
		// to true
		sprite_visible_set(e, false);

		// compute the effective width and number of columns
		int actual_width = img->width - ts->margin * 2;
		int cols = actual_width / (ts->tile_width + ts->spacing);
		actual_width -= (cols - 1) * ts->spacing;
		cols = actual_width / ts->tile_width;

		// number of rows
		int actual_height = img->height - ts->margin * 2;
		int rows = actual_height / (ts->tile_height + ts->spacing);
		actual_height -= (rows - 1) * ts->spacing;
		rows = actual_height / ts->tile_height;

		int tiles = cols * rows;
		spark_log(
			LOG_INFO,
			"TMX sheet info:\n"
			"\trows: %d\n"
			"\tcols: %d\n"
			"\tsize: %dx%d (%dx%d)\n"
			"\ttiles: %d\n"
			"\tspacing: %d\n"
			"\tmargin: %d",
			rows,
			cols,
			img->width,
			img->height,
			actual_width,
			actual_height,
			tiles,
			ts->spacing,
			ts->margin
		);

		// initialize frameset
		int frameset[tiles * 4];
		info->anim_names = g_new0(char*, tiles + 1);
		for (int t = 0; t < tiles; t++) {
			int r = t / cols;
			int c = t % cols;
			int x = ts->margin + c * (ts->tile_width + ts->spacing);
			int y = ts->margin + r * (ts->tile_height + ts->spacing);
			frameset[t * 4] = x;
			frameset[t * 4 + 1] = y;
			frameset[t * 4 + 2] = ts->tile_width;
			frameset[t * 4 + 3] = ts->tile_height;

			char *name = g_strdup_printf("%d", t);
			sprite_animation_add(e, name, &t, 1, 0);
			info->anim_names[t] = name;
		}
		sprite_frameset_set(e, frameset, tiles);
	}

	// wrap the info struct inside a refcounter
	RefCounter *info_rc = ref_counter_new(info, destroy_tmx_map_info);

	// initialize the tilemap component
	tilemap_init(
		e,
		ts->tile_width,
		ts->tile_height,
		map->width,
		map->height,
		update_func,
		info_rc
	);
	ref_counter_dec(info_rc);

	return e;
}

void
tile_initializer(Object *o, void *data)
{
	TMXTile *t = data;
	GHashTableIter iter;
	gpointer key, val;

	g_hash_table_iter_init(&iter, t->props);
	while (g_hash_table_iter_next(&iter, &key, &val)) {
		// FIXME: implement these as properties and not attributes?
		Value attr = {
			.type = SCRIPT_TYPE_STRING,
			.val.string = val
		};
		spark_script_object_attr_set(o, key, &attr);
	}
}

static void
call_script_update_func(int update_type, Tile *t, void *data)
{
	Value *callable = data;

	// convert the update type to a string
	char *upd_type;
	switch (update_type) {
	case TILE_CREATE:
		upd_type = g_strdup("create");
		break;
	case TILE_UPDATE:
		upd_type = g_strdup("update");
		break;
	case TILE_DESTROY:
		upd_type = g_strdup("delete");
	}

	Value args[] = {{
		.type = SCRIPT_TYPE_STRING,
		.val.string = upd_type
	}, {
		.type = SCRIPT_TYPE_OBJECT,
	}};

	RefCounter *data_rc = ref_counter_new(
		g_memdup(t->data, sizeof(TMXTile)), NULL
	);
	tile_object_value_init(&args[1], t, tile_initializer, data_rc);

	spark_script_value_call(callable, 2, args);
}

static Value
wrap_tmx_map_load(int argc, Value *argv)
{
	Value retval;
	Entity e;

	// check args count
	if (argc < 1 || argc > 2) {
		retval.val.string = g_strdup("invalid number of arguments");
		goto bad_args;
	}

	// first arg must be a string with file name
	if (argv[0].type != SCRIPT_TYPE_STRING) {
		retval.val.string = g_strdup("first argument must be a file name");
		goto bad_args;
	}
	const char *filename = argv[0].val.string;

	// second arg, if given, must be a callable
	if (argc == 2) {
		if (argv[1].type != SCRIPT_TYPE_FUNCTION) {
			retval.val.string = g_strdup(
				"second argument must be a callable"
			);
			goto bad_args;
		}

		RefCounter *callable_rc = ref_counter_new(
			g_memdup(&argv[1], sizeof(Value)), NULL
		);

		e = tmx_map_load(
			filename, call_script_update_func, callable_rc
		);
	}
	else {
		e = tmx_map_load(filename, NULL, NULL);
	}

	entity_object_value_init(&retval, e);
	return retval;

bad_args:
	retval.type = SCRIPT_TYPE_ERROR;
	return retval;
}

void
tmx_map_loader_register()
{
	int argtv[] = { SCRIPT_TYPE_VARARG };
	spark_script_func_register(
		"TMXMap",
		"load",
		wrap_tmx_map_load,
		1,
		argtv
	);

	registered = true;
}

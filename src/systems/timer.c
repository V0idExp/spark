#include "common.h"
#include "systems/timer.h"
#include <SDL_timer.h>
#include <glib.h>
#include <stdbool.h>

struct Timer {
	Entity entity;
	int duration;
	Uint32 start_ts;
};

static void
sys_init(struct System *self)
{
	self->data = g_array_new(false, true, sizeof(struct Timer));
}

static void
sys_exit(struct System *self)
{
	g_array_free(self->data, true);
}

static void
sys_update(struct System *self)
{
	GArray *timers = self->data;
	for (int i = 0; i < timers->len; i++) {
		struct Timer *t = &g_array_index(timers, struct Timer, i);
		if (t->duration) {
			Uint32 ts = SDL_GetTicks();
			if (ts - t->start_ts >= t->duration) {
				t->start_ts = t->duration = 0;
				spark_event_push(
					TIMER,
					EVENT_TIMEOUT,
					t->entity,
					NULL,
					0
				);
			}
		}
	}
}

static void
sys_component_create(struct System *self, Entity entity)
{
	struct Timer t = {
		.entity = entity,
		.duration = 0,
		.start_ts = 0,
	};
	g_array_append_val(self->data, t);
}

static void
sys_component_destroy(struct System *self, Entity entity)
{
	GArray *timers = self->data;
	for (int i = 0; i < timers->len; i++) {
		struct Timer *t = &g_array_index(timers, struct Timer, i);
		if (t->entity == entity) {
			g_array_remove_index(timers, i);
			break;
		}
	}
}

static void*
sys_component_get(struct System *self, Entity entity)
{
	GArray *timers = self->data;
	for (int i = 0; i < timers->len; i++) {
		struct Timer *t = &g_array_index(timers, struct Timer, i);
		if (t->entity == entity)
			return t;
	}
	return NULL;
}

extern void
timer_system_register_wrappers();

void
timer_system_register()
{
	spark_assert_has_systems(CORE);

	struct System *sys = g_new0(struct System, 1);
	sys->name = "timer";
	sys->init = sys_init;
	sys->exit = sys_exit;
	sys->update = sys_update;
	sys->create_component = sys_component_create;
	sys->destroy_component = sys_component_destroy;
	sys->get_component = sys_component_get;

	spark_system_register(TIMER, sys);
	timer_system_register_wrappers();
}

void
timer_start(Entity e, int duration)
{
	struct Timer *t = spark_entity_component_get(e, TIMER);
	t->duration = duration;
	t->start_ts = SDL_GetTicks();
}

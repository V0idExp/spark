#pragma once

#include "refcounter.h"
#include "spark.h"
#include <glib.h>
#include <stdbool.h>

typedef struct Animation {
	GArray *frames;
	int current_frame;
	unsigned int last_update;
	int duration;
	bool paused;
} Animation;

typedef struct Sprite {
	struct System *creator;
	Entity entity;
	int x;
	int y;
	int z;
	int width;
	int height;
	bool initialized;
	bool visible;
	struct Animation current_animation;

	// reference-counted SDL_Texture
	RefCounter *texture;

	GArray *frameset;
	GHashTable *animations;
} Sprite;
